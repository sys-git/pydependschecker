[ ![Codeship Status for sys-git/depends](https://www.codeship.io/projects/e6dda830-74ac-0131-8960-72dc6f10bff2/status)](https://www.codeship.io/projects/13780)

depends
================

A pure-python dependency graph creator and checker

Checks hierarchical dependency graphs in the form [(id, [dependency_ids])].
It us up to you to create the graph properly, you can verify it using the 'verify' method
and/or by creating 'depends' objects with the 'strict' kwarg set to 'True'.

In non-strict mode, circular dependencies, failures to add/remove dependencies (because
they might already be added/removed already) will not raise an error.

Make sure all hashable_names are unique - this is used in the dependency lookup.

Example:

```
#!python

    from depends import depends
    #   Create a 'depends' with no dynamic checking
    a = depends('hashable_identifier_a', strict=False)
    print a
    > hashable_a
    
    #   Make 'a' depend on another 'thing1':
    a += 'hashable_b'
    print a
    > {'hashable_a': ['hashable_b']}

    #   'thing1' is now a 'depends' as well
    assert isinstance(a.hashable_b, depends)

    print a.hashable_b in a
    > True

    print 'hashable_b' in a
    > True

    print 'hashable_c' not in a
    > True

    assert a.hashable_b==a['hashable_b]
    >

    assert a.hashable_b is a['hashable_b]
    >

    a.hashable_c
    > Traceback (most recent call last):
    > ...
    > AttributeError: 'dict' object has no attribute 'hashable_c'

    a['hashable_c']
    > Traceback (most recent call last):
    > ...
    > KeyError: 'hashable_c'

    print depends('hashable_c') in a
    > False

    print depends('hashable_c') not in a
    > True

    #   Make 'a' depend on another 'thing2'
    from depends import dep
    dep(a, 'hashable_c')
    print a
    > {'hashable_a': ['hashable_b', 'hashable_c']}

    #   Remove 'thing1' as a dependency of 'a'
    a -= 'hashable_c'
    print a
    > {'hashable_a': ['hashable_b']}

    #   Remove 'thing2' as a dependency of 'a'
    from depends import ndep
    ndep(a, 'hashable_c')
    print a
    > hashable_a

    #   Try and remove 'thing3' as a dependency of 'a'
    a -= 'hashable_c'
    >

    #   Try and remove 'thing4' as a dependency of 'a' by item
    del a['hashable_d']
    >

    #   Try and remove 'thing4' as a dependency of 'a' by attribute
    del a.hashable_d
    >

    #   Enable dynamic dependency checking
    strict(a)
    >

    #   and retry:
    a -= 'hashable_c'
    > Traceback (most recent call last):
    > ...
    > KeyError: 'c'

    del a['hashable_d']
    > Traceback (most recent call last):
    > ...
    > KeyError: 'hashable_d'
    
    del a.hashable_d
    > Traceback (most recent call last):
    > ...
    > KeyError: 'hashable_d'
    
    #   Create a depends with dynamic checking enabled
    b = depends('hashable_identifier_b', strict=True)
    print b
    > STRICT(hashable_b)

    #   'Accidentally' create a circular dependency
    b += b
    > Traceback (most recent call last):
    > ...
    > CircularDependencyError: CIRC(hashable_b)

    #   Enable dynamic checking
    from depends import strict
    strict(a)
    >
    strict(a, True)
    >

    #   Disable dynamic checking
    from depends import nstrict
    nstrict(a)
    >
    strict(a, False)
    >

    #   Perform a verification on-demand
    from depends import verify
    print verify(a)
    > hashable_a

    #   'Accidentally' create a circular dependency
    a += a
    >

    verify(a)
    > Traceback (most recent call last):
    > ...
    > CircularDependencyError: CIRC(hashable_a)

    #   Test for equality between both depends' dependency graphs
    a = depends('hashable_a', strict=False)
    > 
    b = depends('hashable_b', strict=False)
    > 
    assert a!=b
    > 
    c = depends('hashable_b', strict=False)
    > 
    assert b==c
    > 

    #   Iterate over the dependencies:
    a += 'hashable_b'
    >
    a += 'hashable_c'
    >
    print [i for i in a]
    > ['hashable_b', 'hashable_c']

```



In case you're feeling generous:
LTC: LT636SrauWAz9XDz2EKxAXQ5jKqehyhR69
BTC: 13vS6cvzZXf1Yxrar2SYSPQrFLSEwLePV4